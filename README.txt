The Login Landing Page module provides options to specify the landing page to
which a site user is redirected after logging in.

The module handles landing page destinations as follows:

Global login landing page:
Stored as a config setting and administered on the module settings page.

Role-based login landing page:
Stored as a third party setting for roles and administered via
/admin/people/roles
If a user has multiple roles, the landing page specified on the role with the
heaviest row weight will be used.

User-based login landing page:
Stored in a link field 'login_landing_page_link' added to the user entity and
administered via /admin/people

Module settings page:
The module settings page enables the landing page options to be enabled or
disabled and also provides the location to configure the global login landing
page link.
Disabling settings does not hide the additional fields on roles or users but
merely ignores them when processing the redirect in hook_user_login().
