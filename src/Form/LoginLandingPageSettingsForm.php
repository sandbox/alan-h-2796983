<?php

namespace Drupal\login_landing_page\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Main login_landing_page settings admin form.
 */
class LoginLandingPageSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'login_landing_page_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'login_landing_page.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config('login_landing_page.settings');

    $form['login_redirect_enable_users'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('<b>User</b> based login landing page enable'),
      '#default_value' => $config->get('login_landing_page.login_redirect_enable_users'),
      '#description' => $this->t('Provide a login landing page setting in user settings that overrides all other landing page settings'),

    );

    $form['login_redirect_enable_roles'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('<b>Role</b> based login landing page enable'),
      '#default_value' => $config->get('login_landing_page.login_redirect_enable_roles'),
      '#description' => $this->t('Provide a login landing page setting in role settings that overrides the global setting.'),
    );

    $form['login_redirect_enable_global'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('<b>Default</b> login landing page enable'),
      '#default_value' => $config->get('login_landing_page.login_redirect_enable_global'),
      '#description' => $this->t('Enable the global landing page to which all users will be redirected unless overridden by their role or user specific setting'),
    );

    $form['login_redirect_default_link'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Default login landing page destination link'),
      '#default_value' => $config->get('login_landing_page.login_redirect_default_link'),
      '#description' => $this->t("Default landing page destination link for all users that don't have role or user based options set"),
      '#states' => array(
        'visible' => array(
          ':input[name="login_redirect_enable_global"]' => array('checked' => TRUE),
        ),
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!empty($form_state->getValue('login_redirect_enable_global'))) {

      $url = $form_state->getValue('login_redirect_default_link');

      if (empty($url)) {
        $form_state->setError($form['login_redirect_default_link'], $this->t('Please set the landing page destination link'));
      }
      elseif (!(\Drupal::service('path.validator')->getUrlIfValid($url))) {
        $form_state->setError($form['login_redirect_default_link'], $this->t('The landing page destination link is not valid'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('login_landing_page.settings')
      ->set('login_landing_page.login_redirect_default_link',
        $form_state->getValue('login_redirect_default_link'))
      ->set('login_landing_page.login_redirect_enable_users',
        $form_state->getValue('login_redirect_enable_users'))
      ->set('login_landing_page.login_redirect_enable_roles',
        $form_state->getValue('login_redirect_enable_roles'))
      ->set('login_landing_page.login_redirect_enable_global',
        $form_state->getValue('login_redirect_enable_global'))
      ->save();

    parent::submitForm($form, $form_state);

  }

}
